import React from 'react';
import { Button, Container, Accordion, Card, Row, Col } from 'react-bootstrap';

const MembershipTier = () => { return (
  <div id="section2">
    <div className="desktop-view">
    <div className="bg-white">
      <div className="py-5">
        <Container className="container-custom my-5">
            <div className="d-flex flex-column bd-highlight mb-3">
              <div className="p-2 bd-highlight res-text-5xl py-5 tracking-wide text-center">Membership tiers</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-3 text-center">For full access to the community features please choose one of the subscription plans</div>
            </div>
        </Container>
        <Container className="container-custom my-5">
          <Row >
            <Col className="border border-top-0 border-left-0 p-3"></Col>
            <Col className="text-center res-text-lg border border-top-0 font-weight-bold text-custom-4 p-3">Professional</Col>
            <Col className="text-center res-text-lg border border-top-0 font-weight-bold text-custom-4 p-3">All-star</Col>
            <Col className="text-center res-text-lg border border-top-0 font-weight-bold text-custom-4 p-3">Titan</Col>
            <Col className="text-center res-text-lg border border-top-0 font-weight-bold text-custom-4 p-3">Enterprise</Col>
            <Col className="text-center res-text-lg border border-top-0 font-weight-bold text-custom-4 p-3">Investor</Col>
          </Row>
          <Row className="">
            <Col className="res-text-lg text-custom-3 border border-left-0 p-3">Lorem ipsum dolor sit amet consectetur</Col>
            <Col className="d-flex align-items-center justify-content-center text-center z res-text-lg text-custom-3 border p-3">-</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">-</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">-</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">-</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">0.1%</Col>
          </Row>
          <Row className="">
            <Col className="res-text-lg text-custom-3 border border-left-0 p-3">Lorem ipsum dolor sit amet consectetur</Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
          </Row>
          <Row className="">
            <Col className="res-text-lg text-custom-3 border border-left-0 p-3">Lorem ipsum dolor</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">50% discount </Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Free</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Free</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Free</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Free</Col>
          </Row>
          <Row className="">
            <Col className="res-text-lg text-custom-3 border border-left-0 p-3">Lorem ipsum dolor</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">25% discount</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">2 complimentary</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">6 complimentary</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">6 complimentary</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">6 complimentary</Col>
          </Row>
          <Row className="">
            <Col className="res-text-lg text-custom-3 border border-left-0 p-3">Lorem ipsum dolor sit amet consectetur</Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
          </Row>
          <Row className="">
            <Col className="res-text-lg text-custom-3 border border-left-0 p-3">Lorem ipsum dolor sit amet consectetur</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Send up to 10 </Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Send up to 20</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Send up to 60</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Send up to 60</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">Send up to 60</Col>
          </Row>
          <Row className="">
            <Col className="res-text-lg text-custom-3 border border-left-0 p-3">Lorem ipsum dolor sit amet consectetur</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">-</Col>
            <Col className="d-flex align-items-center justify-content-center text-center res-text-lg text-custom-3 border p-3">-</Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
            <Col className="d-flex align-items-center justify-content-center res-text-lg text-center border p-3">
              <img className="h-3 w-3" src={require('../../common/images/check.png')} />
            </Col>
          </Row>
          <Row className="">
            <Col className="border border-left-0 border-bottom-0 p-3"></Col>
            <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between bd-highlight h-full">
                        <div className="text-center res-text-lg text-custom-3 font-weight-bold bd-highlight">$20 / month</div>
                        <Button className="" variant="common"><b>Select plan</b></Button>
            </Col>
            <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between bd-highlight h-full">
                        <div className="text-center res-text-lg text-custom-3 font-weight-bold bd-highlight">$50 / month</div>
                        <Button className="" variant="common"><b>Select plan</b></Button>
            </Col>
            <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between bd-highlight h-full">
                        <div className="text-center res-text-lg text-custom-3 font-weight-bold bd-highlight">$100 / month</div>
                        <Button className="" variant="common"><b>Select plan</b></Button>
            </Col>
            <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between bd-highlight h-full">
                        <div className="d-flex flex-column bd-highlight">
                          <div className="text-center res-text-lg text-custom-3 leading-snug font-weight-bold bd-highlight">$500 / month</div>
                          <div className="text-center text-smbase text-custom-2 bd-highlight leading-snug">(up to 10 users)</div>
                        </div>
                        <Button className="" variant="common"><b>Select plan</b></Button>
            </Col>
            <Col className="border border-bottom-0 p-3 d-flex flex-column justify-content-between bd-highlight h-full">
                        <div className="d-flex flex-column bd-highlight">
                          <div className="text-center res-text-lg text-custom-3 font-weight-bold bd-highlight">$175 / month</div>
                          <div className="text-center text-smbase text-custom-2 bd-highlight leading-snug">for 12 months</div>
                          <div className="text-center text-smbase text-custom-2 bd-highlight leading-snug pt-1 pb-3">$2000 as a one off payment (save $100)</div>
                        </div>
                        <Button className="res-text-lg" variant="common"><b>Select plan</b></Button>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  </div>
    <div className="mobile-view">
      <div className="bg-white">
        <div className="pb-5 mx-3">
          <Container className=" my-5">
              <div className="d-flex flex-column bd-highlight mb-3">
                <div className="p-2 bd-highlight text-3xl py-4 tracking-wide text-center">Membership tiers</div>
                <div className="p-2 bd-highlight res-text-xl text-custom-3 py-2 text-center">For full access to the community features please choose one of the subscription plans</div>
              </div>
          </Container>
          <Accordion defaultActiveKey="0">
            <Container>
              <Row >
                <Col className="p-2 text-center border position-relative" >              
                  <Accordion.Toggle as={Card.Header} className="accordion-header" eventKey="0">
                    <div className="text-center d-flex justify-content-center h-full">
                      <div className="text-lg text-center font-weight-bold text-custom-4 ">Professional</div>
                    </div>
                  </Accordion.Toggle>
                  <Accordion.Toggle className="position-absolute arrow-pos" as={Button} variant="transparent" eventKey="0">
                    <img className="w-2 h-2 " src={require('../../common/images/arrow-down.png')} />
                  </Accordion.Toggle>
                </Col>
              </Row>
              <Accordion.Collapse eventKey="0">
                <div>            
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <img className="h-2 w-2 mx-auto my-1" src={require('../../common/images/check.png')} />
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">50% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">25% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <img className="h-2 w-2 mx-auto my-1" src={require('../../common/images/check.png')} />
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="text-center text-lg text-custom-3 font-weight-bold border p-3">$20 / month</Col>
                  </Row>
                  <Row className="">
                    <Col className="border p-3 text-center d-flex flex-column h-full">
                      <Button className="" variant="common"><b>Select plan</b></Button>
                    </Col>
                  </Row>
                </div>
              </Accordion.Collapse>
            </Container>
            <Container>
              <Row >
                <Col className="p-2 text-center border position-relative" >              
                  <Accordion.Toggle as={Card.Header} className="accordion-header" eventKey="1">
                    <div className="text-center d-flex justify-content-center h-full">
                      <div className="text-lg font-weight-bold text-custom-4 ">All-star</div>
                    </div>
                  </Accordion.Toggle>
                  <Accordion.Toggle className="position-absolute arrow-pos" as={Button} variant="transparent" eventKey="1">
                    <img className="w-2 h-2" src={require('../../common/images/arrow-down.png')} />
                  </Accordion.Toggle>
                </Col>
              </Row>
              <Accordion.Collapse eventKey="1">
                <div>            
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">50% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">25% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="text-center text-lg text-custom-3 font-weight-bold border p-3">$20 / month</Col>
                  </Row>
                  <Row className="">
                    <Col className="border p-3 text-center d-flex flex-column h-full">
                      <Button className="" variant="common"><b>Select plan</b></Button>
                    </Col>
                  </Row>
                </div>
              </Accordion.Collapse>
            </Container>
            <Container>
              <Row>
                <Col className="p-2 text-center border position-relative" >              
                  <Accordion.Toggle as={Card.Header} className="accordion-header" eventKey="2">
                    <div className="text-center d-flex justify-content-center h-full">
                      <div className="text-lg font-weight-bold text-custom-4 ">Titan</div>
                    </div>
                  </Accordion.Toggle>
                  <Accordion.Toggle className="position-absolute arrow-pos" as={Button} variant="link" eventKey="2">
                    <img className="w-2 h-2" src={require('../../common/images/arrow-down.png')} />
                  </Accordion.Toggle>
                </Col>
              </Row>
              <Accordion.Collapse eventKey="2">
                <div>            
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">50% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">25% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="text-center text-lg text-custom-3 font-weight-bold border p-3">$20 / month</Col>
                  </Row>
                  <Row className="">
                    <Col className="border p-3 text-center d-flex flex-column h-full">
                      <Button className="" variant="common"><b>Select plan</b></Button>
                    </Col>
                  </Row>
                </div>
              </Accordion.Collapse>
            </Container>
            <Container>
              <Row>
                <Col className="p-2 text-center border position-relative" >              
                  <Accordion.Toggle as={Card.Header} className="accordion-header" eventKey="3">
                    <div className="text-center d-flex justify-content-center h-full">
                      <div className="text-lg font-weight-bold text-custom-4 ">Enterprise</div>
                    </div>
                  </Accordion.Toggle>
                  <Accordion.Toggle className="position-absolute arrow-pos" as={Button} variant="link" eventKey="3">
                    <img className="w-2 h-2" src={require('../../common/images/arrow-down.png')} />
                  </Accordion.Toggle>
                </Col>
              </Row>
              <Accordion.Collapse eventKey="3">
                <div>            
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">50% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">25% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="text-center text-lg text-custom-3 font-weight-bold border p-3">$20 / month</Col>
                  </Row>
                  <Row className="">
                    <Col className="border p-3 text-center d-flex flex-column h-full">
                      <Button className="" variant="common"><b>Select plan</b></Button>
                    </Col>
                  </Row>
                </div>
              </Accordion.Collapse>
            </Container>
            <Container>
              <Row>
                <Col className="p-2 text-center border position-relative" >              
                  <Accordion.Toggle as={Card.Header} className="accordion-header" eventKey="4">
                    <div className="text-center d-flex justify-content-center h-full">
                      <div className="text-lg font-weight-bold text-custom-4 ">Investor</div>
                    </div>
                  </Accordion.Toggle>
                  <Accordion.Toggle className="position-absolute arrow-pos" as={Button} variant="link" eventKey="4">
                    <img className="w-2 h-2" src={require('../../common/images/arrow-down.png')} />
                  </Accordion.Toggle>
                </Col>
              </Row>
              <Accordion.Collapse eventKey="4">
                <div>            
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">50% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">25% discount</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 font-weight-bold py-1">Send up to 10</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="border border-bottom-0 p-3 text-center d-flex flex-column justify-content-between h-full">
                      <div className="text-base text-custom-3 py-1">-</div>
                      <div className="text-base text-custom-3 py-1">Lorem ipsum dolor sit amet</div>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col className="text-center text-lg text-custom-3 font-weight-bold border p-3">$20 / month</Col>
                  </Row>
                  <Row className="">
                    <Col className="border p-3 text-center d-flex flex-column h-full">
                      <Button className="" variant="common"><b>Select plan</b></Button>
                    </Col>
                  </Row>
                </div>
              </Accordion.Collapse>
            </Container>
          </Accordion>
        </div>
      </div>
    </div>
  </div>
  );}

  export default MembershipTier
