import React from 'react';
import { Button, Container, Form, Row,  Col } from 'react-bootstrap';

const BgHome = () => { return (
  <div id="section5">
    <div className="desktop-view">
      <div className="position-relative home">
        <img className="position-absolute w-100 h-800 z-0" src={require('../../common/images/Homepage.jpg')} />
        <div className="position-absolute position d-flex flex-column justify-content-between h-100 left-section-home pb-4 z-30">
          <div></div>
          <div className="d-flex flex-column mb-3">
            <div className="py-2">
            <img className="logo-home" src={require('../../common/images/logo.png')} />
            </div>
            <div className="p-2 desc-home leading-normal tracking-wider my-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</div>
          </div>
          <div className="d-flex flex-row my-3 px-2">
            <div className="py-2 user-select-none res-text-xl">Already have an account?</div>
            <div className="py-2 px-1 cursor-pointer res-text-xl font-weight-bold text-custom-4">Sign in </div>
          </div>
        </div>
    
        <div className="position-absolute position-2 d-flex flex-column right-section-home mb-3 z-30">
          <div className="py-3 user-select-none tracking-wide text-custom-3 res-text-2-3xl">Create an account</div>
          <Form className="mt-2">
            <Form.Group controlId="exampleForm.ControlInput1">
              <div className="text-custom-2 title-user-input user-select-none py-1">Name</div>
              <input className="user-input text-lg pb-2" type="name" value="John Doe" ></input>
            </Form.Group>
    
            <Form.Group controlId="formBasicEmail">
              <div className="text-custom-2 text-smbase user-select-none py-1">Email</div>
              <input className="user-input text-lg pb-2" type="email" value="johndoe@dummy.com" ></input>
            </Form.Group>
    
            <Form.Group controlId="formBasicPassword">
              <div className="text-custom-2 text-smbase user-select-none py-1">Password</div>
              <input className="user-input text-lg pb-2" type="password" value="test" ></input>
            </Form.Group>
    
            <Form.Group controlId="exampleForm.ControlInput2">
              <div className="text-custom-2 text-smbase user-select-none py-1">Company</div>
              <input className="user-input text-lg pb-2" type="name" value="The Lorem Ipsum Company" ></input>
            </Form.Group>
    
            <Form.Group controlId="exampleForm.ControlInput2">
              <div className="text-custom-2 text-smbase user-select-none py-1">Job title</div>
              <input className="user-input text-lg pb-2" type="name" value="CEO" ></input>
            </Form.Group>
    
            <Button className="w-100 py-3 px-4 res-my-3" variant="common" type="submit">
              <b className="res-text-lg">Create an account</b>
            </Button>
          </Form>
    
          <div className="d-flex justify-content-center my-2">
            <div className="py-2 flex-fill"><hr/></div>
            <div className="py-2 px-3 text-custom-2 res-text-sm user-select-none align-self-center">OR</div>
            <div className="py-2 flex-fill"><hr/></div>
          </div>
    
          <Row>
            <Col>        
              <Button className="w-100 res-py-3 px-4" variant="linkedin"><b className="res-text-base">Log in with LinkedIn</b></Button>
            </Col>
            <Col>
              <Button className="w-100 res-py-3 px-4" variant="google"><b className="res-text-base">Log in with Google</b></Button>
            </Col>
          </Row>
        </div>
      </div>
    </div>
    <div className="mobile-view">
      <Container className="py-4">
        <Row>
          <Col><img className="w-64 my-2" src={require('../../common/images/logo.png')} /></Col>
        </Row>
        <Row>
          <Col className="text-xl m-2 ml-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit</Col>
        </Row>
        <Row>
          <Col className="m-2">
            <div className="user-select-none tracking-wide text-custom-3 text-2xl my-2">Create an account</div>
            <Form className="mt-2">
              <Form.Group controlId="exampleForm.ControlInput1">
                <div className="text-custom-2 user-select-none py-1">Name</div>
                <input className="user-input text-xl pb-2" type="name" value="John Doe" ></input>
              </Form.Group>
      
              <Form.Group controlId="formBasicEmail">
                <div className="text-custom-2 user-select-none py-1">Email</div>
                <input className="user-input text-lg pb-2" type="email" value="johndoe@dummy.com" ></input>
              </Form.Group>
      
              <Form.Group controlId="formBasicPassword">
                <div className="text-custom-2 user-select-none py-1">Password</div>
                <input className="user-input text-lg pb-2" type="password" value="test" ></input>
              </Form.Group>
      
              <Form.Group controlId="exampleForm.ControlInput2">
                <div className="text-custom-2 user-select-none py-1">Company</div>
                <input className="user-input text-lg pb-2" type="name" value="The Lorem Ipsum Company" ></input>
              </Form.Group>
      
              <Form.Group controlId="exampleForm.ControlInput2">
                <div className="text-custom-2 user-select-none py-1">Job title</div>
                <input className="user-input text-lg pb-2" type="name" value="CEO" ></input>
              </Form.Group>
      
              <Button className="w-100 py-3 px-4 my-2" variant="common" type="submit">
                <b className="res-text-lg">Create an account</b>
              </Button>
            </Form>
            <div className="d-flex justify-content-center my-1">
              <div className="py-2 flex-fill"><hr/></div>
              <div className="py-2 px-3 text-custom-2 res-text-sm user-select-none align-self-center">OR</div>
              <div className="py-2 flex-fill"><hr/></div>
            </div>
            <Button className="w-100 py-3 px-4 my-2" variant="linkedin"><b className="res-text-lg">Log in with LinkedIn</b></Button>
            <Button className="w-100 py-3 px-4 my-2" variant="google"><b className="res-text-lg">Log in with Google</b></Button>
          </Col>
        </Row>
      </Container>
    </div>
  </div>
  );}

  export default BgHome