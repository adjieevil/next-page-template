import React from 'react';
import { Container } from 'react-bootstrap';

const NavBar = () => { return (
  <div>
    <div className="desktop-view">
      <Container className="p-3">
        <div className="d-flex justify-content-center">
          <a href="#section1" className="p-3 mx-4 text-custom-5 cursor-pointer res-text-xl">About us</a>
          <a href="#section2" className="p-3 mx-4 text-custom-5 cursor-pointer res-text-xl">Membership tiers</a>
          <a href="#section3" className="p-3 mx-4 text-custom-5 cursor-pointer res-text-xl">Contact us</a>
          <a href="#section4" className="p-3 mx-4 text-custom-5 cursor-pointer res-text-xl">For investors</a>
          <a href="#section5" className="p-3 mx-4 text-custom-5 cursor-pointer res-text-xl">Create an account</a>
        </div>
      </Container>
    </div>
    <div className="mobile-view">
      <div></div>
    </div>
  </div>
  );}
  
export default NavBar