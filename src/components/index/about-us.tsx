import React from 'react';
import {Container } from 'react-bootstrap';

const AboutUs = () => { return (
  <div id="section1">
  <div className="desktop-view">
    <div className="bg-custom-1">
      <div className="py-5">
        <Container>
            <div className="d-flex flex-column bd-highlight mb-3 px-5">
              <div className="p-2 bd-highlight res-text-5xl py-5 tracking-wide text-center">About us</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-3 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-3 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-3 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua  Ut enim ad minim veniam, </div>
            </div>
        </Container>
        <Container className="container-custom my-5" >
          <div className="position-relative d-flex justify-content-start my-5">
            <img src={require('../../common/images/Learning-development.png')} />
            <div className="left-pos d-flex flex-column bg-white rounded shadow-sm w-104 p-4">
              <div className="p-2 res-text-2-3xl my-2">Learning & development</div>
              <div className="p-2 res-text-xl my-2">Lorem ipsum dolor sit amet,<br/>consectetur adipiscing elit</div>
            </div>
          </div>
          <div className="position-relative d-flex justify-content-end my-5">
            <img src={require('../../common/images/Get-connected.png')} />
            <div className="right-pos d-flex flex-column bg-white rounded shadow-sm w-104 p-4">
              <div className="p-2 res-text-2-3xl my-2">Get connected</div>
              <div className="p-2 res-text-xl my-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
            </div>
          </div>
          <div className="position-relative d-flex justify-content-start my-5">
            <img src={require('../../common/images/Knowledge-sharing.png')} />
            <div className="left-pos d-flex flex-column bg-white rounded shadow-sm w-104 p-4">
              <div className="p-2 res-text-2-3xl my-2">Knowledge sharing</div>
              <div className="p-2 res-text-xl my-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
            </div>
          </div>
          <div className="position-relative d-flex justify-content-end my-5">
            <img src={require('../../common/images/Learning-development.png')} />
            <div className="right-pos d-flex flex-column bg-white rounded shadow-sm w-104 p-4">
              <div className="p-2 res-text-2-3xl my-2">Be in the loop</div>
              <div className="p-2 res-text-xl my-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  </div>
  <div className="mobile-view">
    <div className="bg-custom-1">
      <div className="py-5">
        <Container>
            <div className="d-flex flex-column px-4">
              <div className="p-2 bd-highlight text-3xl py-4 tracking-wide text-center">About us</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-2 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-2 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-2 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua  Ut enim ad minim veniam, </div>
            </div>
        </Container>
        <Container className="my-4" >
          <div className="position-relative d-flex my-5 py-3">
            <img className="w-100" src={require('../../common/images/Learning-development.png')} />
            <div className="res-right-pos d-flex flex-column bg-white rounded shadow-sm p-3 pr-5">
              <div className="p-1 text-xl">Learning & development</div>
              <div className="p-1 text-base">Lorem ipsum dolor sit amet,<br/>consectetur adipiscing elit</div>
            </div>
          </div>
          <div className="position-relative d-flex my-5 py-3">
            <img className="w-100" src={require('../../common/images/Get-connected.png')} />
            <div className="res-left-pos d-flex flex-column bg-white rounded shadow-sm p-3 pr-5">
              <div className="p-1 text-xl">Get connected</div>
              <div className="p-1 text-base">Lorem ipsum dolor sit amet,<br/>consectetur adipiscing elit</div>
            </div>
          </div>
          <div className="position-relative d-flex my-5 py-3">
            <img className="w-100" src={require('../../common/images/Knowledge-sharing.png')} />
            <div className="res-right-pos d-flex flex-column bg-white rounded shadow-sm p-3 pr-5">
              <div className="p-1 text-xl">Knowledge sharing</div>
              <div className="p-1 text-base">Lorem ipsum dolor sit amet,<br/>consectetur adipiscing elit</div>
            </div>
          </div>
          <div className="position-relative d-flex justify-content-end my-5 py-3">
            <img className="w-100" src={require('../../common/images/Be-in-the-loop.png')} />
            <div className="res-left-pos d-flex flex-column bg-white rounded shadow-sm p-3 pr-5">
              <div className="p-1 text-xl">Be in the loop</div>
              <div className="p-1 text-base">Lorem ipsum dolor sit amet, <br/> consectetur adipiscing elit Lorem <br/> ipsum dolor</div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  </div>
  </div>
  );}

  export default AboutUs