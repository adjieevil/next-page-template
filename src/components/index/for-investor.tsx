import React from 'react';
import { Button, Container, Col } from 'react-bootstrap';

const ForInvestor = () => { return (
  <div id="section4">
    <div className="desktop-view">
      <div className="bg-white ">
          <Container className=" my-5 border-top">
          <div className="py-5">
            <div className="d-flex flex-column">
              <div className="p-2 bd-highlight res-text-5xl py-5 tracking-wide text-center">For investors</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-2 text-center">Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur</div>
              <Col className="d-flex justify-content-center">
                <Button className="px-4 py-2 my-5" variant="common" type="submit"><b className="res-text-lg">Download pitch deck</b></Button>
              </Col>
            </div>
          </div>
          </Container>
      </div>
    </div>
    <div className="mobile-view">
      <div className="bg-white ">
          <Container className=" my-5 border-top">
          <div className="py-5">
            <div className="d-flex flex-column">
              <div className="p-2 bd-highlight text-3xl py-4 tracking-wide text-center">For investors</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-2 mx-4 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
              <Col className="d-flex">
                <Button className="px-4 py-2 my-5 w-100" variant="common" type="submit"><b className="res-text-lg">Download pitch deck</b></Button>
              </Col>
            </div>
          </div>
          </Container>
      </div>
    </div>
  </div>
  );}

export default ForInvestor