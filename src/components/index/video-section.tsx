import React from 'react';
import ReactPlayer from 'react-player'

const VideoSection = () => { return (
  <div>
    <div className="desktop-view">
      <div >
        {/* <iframe className="video" src="https://www.youtube.com/embed/tgbNymZ7vqY">
        </iframe> */}
    
        <ReactPlayer  width="100%" height="720px" light={true} playing={true} controls={true} url='https://www.youtube.com/watch?v=xi6r3hZe5Tg' />
    
      </div>
    </div>
    <div className="mobile-view">
      <div >
        {/* <iframe className="video" src="https://www.youtube.com/embed/tgbNymZ7vqY">
        </iframe> */}
    
        <ReactPlayer  width="100%" height="420px" playing={false} controls={true} url='https://www.youtube.com/watch?v=xi6r3hZe5Tg' />
    
      </div>
    </div>
  </div>
  );}

  export default VideoSection