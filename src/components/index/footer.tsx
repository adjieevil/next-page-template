import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const Footer = () => {
  const [isFacebook, setFacebook] = useState(false);
  const [isLinkedin, setLinkedin] = useState(false);
  const [isTwitter, setTwitter] = useState(false);
  const [isYoutube, setYoutube] = useState(false);

  return (
  <div>
    <div className="desktop-view">
      <div className="bg-custom-1">
        <div className="py-5">
          <Container className="container-custom-2 pb-5">
              <div className="d-flex justify-content-between bd-highlight mb-3">
                <div className="text-center text-custom-2 bd-highlight">© 2020 by Lorem</div>
                <div className="d-flex flex-row bd-highlight mb-3">
                  {isFacebook ? 
                    <img onMouseEnter={() => setFacebook(true)} onMouseLeave={() => setFacebook(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/facebook.png')} /> :
                    <img onMouseEnter={() => setFacebook(true)} onMouseLeave={() => setFacebook(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/facebook.png')} />
                  }

                  {isLinkedin ? 
                    <img onMouseEnter={() => setLinkedin(true)} onMouseLeave={() => setLinkedin(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/linkedin.png')} /> :
                    <img onMouseEnter={() => setLinkedin(true)} onMouseLeave={() => setLinkedin(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/linkedin.png')} />
                  }

                  {isTwitter ? 
                    <img onMouseEnter={() => setTwitter(true)} onMouseLeave={() => setTwitter(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/twitter.png')} /> :
                    <img onMouseEnter={() => setTwitter(true)} onMouseLeave={() => setTwitter(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/twitter.png')} />
                  }

                  {isYoutube ? 
                    <img onMouseEnter={() => setYoutube(true)} onMouseLeave={() => setYoutube(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/youtube.png')} /> :
                    <img onMouseEnter={() => setYoutube(true)} onMouseLeave={() => setYoutube(false)} className="w-3 h-3 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/youtube.png')} />
                  }
                </div>
              </div>
          </Container>
        </div>
      </div>
    </div>
    <div className="mobile-view">
      <div className="bg-custom-1">
        <div className="py-5">
          <Container className="container-custom-2 pb-5">
            <Row>
              <Col>              
                <div className="d-flex flex-row justify-content-center mb-3">
                  {isFacebook ? 
                    <img onMouseEnter={() => setFacebook(true)} onMouseLeave={() => setFacebook(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/facebook.png')} /> :
                    <img onMouseEnter={() => setFacebook(true)} onMouseLeave={() => setFacebook(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/facebook.png')} />
                  }

                  {isLinkedin ? 
                    <img onMouseEnter={() => setLinkedin(true)} onMouseLeave={() => setLinkedin(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/linkedin.png')} /> :
                    <img onMouseEnter={() => setLinkedin(true)} onMouseLeave={() => setLinkedin(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/linkedin.png')} />
                  }

                  {isTwitter ? 
                    <img onMouseEnter={() => setTwitter(true)} onMouseLeave={() => setTwitter(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/twitter.png')} /> :
                    <img onMouseEnter={() => setTwitter(true)} onMouseLeave={() => setTwitter(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/twitter.png')} />
                  }

                  {isYoutube ? 
                    <img onMouseEnter={() => setYoutube(true)} onMouseLeave={() => setYoutube(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/youtube.png')} /> :
                    <img onMouseEnter={() => setYoutube(true)} onMouseLeave={() => setYoutube(false)} className="w-2 h-2 mx-3 cursor-pointer img-gray" src={require('../../common/images/active/youtube.png')} />
                  }
                </div>
              </Col>
            </Row>
            <Row>
              <Col>  
                <div className="text-center textt-smbase text-custom-2 py-3 bd-highlight">© 2020 by Lorem</div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    </div>
  </div>
  );}

  export default Footer