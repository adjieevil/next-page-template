import React from 'react';
import { Button, Container, Form, Row,  Col } from 'react-bootstrap';

const ContactUs = () => { return (
  <div id="section3">
    <div className="desktop-view">
    <div className="bg-white">
      <div className="py-5">
        <Container className=" my-5">
            <div className="d-flex flex-column bd-highlight mb-3">
              <div className="p-2 bd-highlight res-text-5xl py-4 tracking-wide text-center">Contact us</div>
              <div className="p-2 bd-highlight res-text-xl text-custom-3 py-4 text-center">Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur</div>
            </div>
            <Form className="my-5">
              <Row>
                <Col>            
                  <Form.Row>
                    <Form.Group as={Col} md="12" controlId="validationCustom01">
                      <div className="text-custom-2 text-smbase user-select-none py-1">Name</div>
                      <input className="user-input text-lg pb-2" type="name" value="John Doe" ></input>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col} md="12" controlId="validationCustom03">
                      <div className="text-custom-2 text-smbase user-select-none py-1">Email</div>
                      <input className="user-input text-lg pb-2" type="email" value="johndoe@dummy.com" ></input>
                    </Form.Group>
                  </Form.Row>
                </Col>
                <Col>
                <Form.Group className="h-100" controlId="exampleForm.ControlTextarea1">
                  <div className="text-custom-2 text-smbase user-select-none py-1">Message</div>
                  {/* <Form.Control className="h-73" as="textarea" /> */}
                  <textarea className="user-input text-lg pb-2 h-73" value="Leave us a message..." ></textarea>
                </Form.Group>
                </Col>
              </Row>
              <Row className="my-3">
                <Col></Col>
                <Col className="d-flex justify-content-end">
                  <Button className="w-100 py-3" variant="common" type="submit"><b className="res-text-lg">Send message</b></Button>
                </Col>
              </Row>
            </Form>
  
        </Container>
      </div>
    </div>
    </div>
    <div className="mobile-view">
      <div className="bg-white">
        <div className="py-4">
          <Container className=" my-4">
              <div className="d-flex flex-column bd-highlight mb-3">
                <div className="p-2 bd-highlight text-3xl py-4 tracking-wide text-center">Contact us</div>
                <div className="p-2 bd-highlight res-text-xl text-custom-3 py-4 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,</div>
              </div>
              <Form className="my-5 mx-3">
                <Row>
                  <Col>            
                    <Form.Row>
                      <Form.Group as={Col} md="12" controlId="validationCustom01">
                        <div className="text-custom-2 text-smbase user-select-none py-1">Name</div>
                        <input className="user-input text-lg pb-2" type="name" value="John Doe" ></input>
                      </Form.Group>
                    </Form.Row>
                    <Form.Row>
                      <Form.Group as={Col} md="12" controlId="validationCustom03">
                        <div className="text-custom-2 text-smbase user-select-none py-1">Email</div>
                        <input className="user-input text-lg pb-2" type="email" value="johndoe@dummy.com" ></input>
                      </Form.Group>
                    </Form.Row>
                    <Form.Row>
                      <Form.Group className="h-100" as={Col} md="12" controlId="exampleForm.ControlTextarea1">
                        <div className="text-custom-2 text-smbase user-select-none py-1">Message</div>
                        {/* <Form.Control className="h-73" as="textarea" /> */}
                        <textarea className="user-input text-lg pb-4 h-73" value="Leave us a message..." ></textarea>
                    </Form.Group>
                    </Form.Row>
                  </Col>
                </Row>
                <Row className="my-3">
                  <Col className="d-flex">
                    <Button className="w-100 py-2" variant="common" type="submit"><b className="res-text-lg">Send message</b></Button>
                  </Col>
                </Row>
              </Form>
    
          </Container>
        </div>
      </div>
    </div>
  </div>
  );}

  export default ContactUs