import React from 'react';

import BgHome from '@components/index/bg-home';
import NavBar from '@components/index/navbar';
import AboutUs from '@components/index/about-us';
import MembershipTier from '@components/index/membership-tier';
import VideoSection from '@components/index/video-section';
import ContactUs from '@components/index/contact-us';
import ForInvestor from '@components/index/for-investor';
import Footer from '@components/index/footer';

const HomePage = () => {
  return (
    <div className="overflow-hidden">
      <style type="text/css">
        {`
        .btn-common {
          border-color: #43a5dc;
          background-color: #43a5dc;
          color: white;
        }

        .btn-common:hover {
          border-color: #378dbf;
          background-color: #378dbf;
          color: white;
        }

        .btn-transparent {
          border-color: transparent;
          background-color: transparent;
          color: transparent;
        }

        .btn-google {
          border-color: #ec5950;
          background-color: transparent;
          color: #ec5950;
        }

        .btn-google:hover {
          color: white;
          background-color: #ec5950;
          border-color: #ec5950;
        }

        .btn-linkedin {
          border-color: #007ab8;
          background-color: transparent;
          color: #007ab8;
        }

        .btn-linkedin:hover {
          color: white;
          background-color: #007ab8;
          border-color: #007ab8;
        }
        `}
      </style>
      <BgHome />
      <NavBar />
      <AboutUs />
      <MembershipTier />
      <VideoSection />
      <ContactUs />
      <ForInvestor />
      <Footer />
  </div>
);
}

export default HomePage
