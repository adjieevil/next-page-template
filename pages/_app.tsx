import React, { FC } from 'react'
import { AppProps } from 'next/app'
// import { storeWrapper } from '@store/store'
import '@common/css/index.css'
// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const CustomApp: FC<AppProps> = ({ Component, pageProps }) => (
  <Component {...pageProps} />
)

export default CustomApp
